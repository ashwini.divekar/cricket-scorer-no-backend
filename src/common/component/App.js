import React from 'react';
import Scorer from '../../features/scorer/component/Scorer';
import { HashRouter, Route, Switch } from 'react-router-dom';
import ScoreDetails from '../../features/scoreDetails/components/ScoreDetails';

const App = () => (
  <HashRouter>
    <Switch>
      <Route exact path="/" component={Scorer} />
      <Route exact path="/scoreDetails" component={ScoreDetails} />
    </Switch>
  </HashRouter>
);

export default App;
