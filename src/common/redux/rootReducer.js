import { combineReducers } from 'redux';
import {default as gameDetails} from '../../features/gameDetails/redux/gameDetailsReducer';
import {default as overDetails} from '../../features/scorer/redux/overDetailsReducer';
import {default as currentBall} from '../../features/scorer/redux/currentBallReducer';
import {default as selectPlayerModal} from '../../features/scorer/redux/selectPlayerModalReducer';

const reducerMap = {
  gameDetails,
  overDetails,
  currentBall,
  selectPlayerModal,
};

export default combineReducers(reducerMap);