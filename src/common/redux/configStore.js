import rootReducer from './rootReducer';
import {applyMiddleware, createStore} from 'redux';
import {createLogger} from 'redux-logger';
import {composeWithDevTools} from 'redux-devtools-extension';
import {checkNextBallMiddleware} from '../../features/scorer/redux/checkNextBallMiddleware';

export default function configStore() {
  let storeEnhancer = undefined;
  if (process.env.NODE_ENV !== 'PRODUCTION') {
    storeEnhancer = composeWithDevTools(applyMiddleware(createLogger({ collapsed: true }), checkNextBallMiddleware));
  }

  return createStore(rootReducer, storeEnhancer);
}
