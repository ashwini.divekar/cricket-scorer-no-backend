export const create2Overs = () => [{
  balls: Array(3).fill({ runs: 4, extras: [], batsmanName:  'Player1' })
    .concat(Array(3).fill({ runs: 3, extras: [], batsmanName:  'Player2' })),
  bowlerName: 'PlayerA'
}, {
  balls: Array(3).fill({ runs: 3, extras: [], batsmanName:  'Player3' })
    .concat(Array(3).fill({ runs: 6, extras: [], batsmanName:  'Player4' })),
  bowlerName: 'PlayerB'
}];

export const create3Overs = () => [{
  balls: Array(3).fill({ runs: 4, extras: [], batsmanName:  'Player1' })
    .concat(Array(3).fill({ runs: 3, extras: [], batsmanName:  'Player2' })),
  bowlerName: 'PlayerB'
}, {
  balls: Array(3).fill({ runs: 3, extras: [], batsmanName:  'Player3' })
    .concat(Array(2).fill({ runs: 6, extras: [], batsmanName:  'Player4' })),
  bowlerName: 'PlayerA'
}, {
  balls: Array(6).fill({ runs: 0, extras: [], batsmanName:  'Player3' }),
  bowlerName: 'PlayerB'
}];

export const createGameDetails = () => ({
  teams: [{
    name: 'Team 1',
    isBatting: true,
    players: [
      { name: 'Player1', isBatting: true },
      { name: 'Player2', isBatting: true },
      { name: 'Player3' },
      { name: 'Player4' },
    ]
  },
  {
    name: 'Team 2',
    isBatting: false,
    players: [
      {name: 'PlayerA', isBowling: true},
      {name: 'PlayerB'},
      {name: 'PlayerC'}
    ],
  }
  ]});
