import React from 'react';
import { create2Overs, createGameDetails } from '../../../testHelpers/testDataSetupHelper';
import {StyledBattingTable} from './BattingTable';
import { mount } from 'enzyme';

describe('Batting Table tests', () => {
  it('should create 3 rows for 3 batsman who have played', () => {
    const props = {
      overDetails: create2Overs(),
      gameDetails: createGameDetails(),
    };

    const BattingTable = mount(<StyledBattingTable {...props}/>);

    // console.log(BattingTable.debug());

    const columns = BattingTable.find('td').map(node => node.text());
    expect(columns).toEqual([
      'Player1*', '12', '3', '3', '0', '400.00',
      'Player2*', '9' , '3', '0', '0', '300.00',
      'Player3',  '9' , '3', '0', '0', '300.00',
      'Player4',  '18', '3', '0', '3', '600.00'
    ]);
  });
});