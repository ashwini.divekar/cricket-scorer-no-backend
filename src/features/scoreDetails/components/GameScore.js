import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography/Typography';
import { GameDetailsPropTypes, TeamDetailsPropTypes } from '../../gameDetails/redux/gameDetailsPropTypes';

const styles = {
  gameScore: {
    textAlign: 'right'
  }
};

const hasTeamNotPlayedYet = (score) => (score.oversPlayed.ballNumber === 0 && score.oversPlayed.overNumber === 0);

const TeamScore = ({ classes, team, isCurrentPlaying, totalOversInGame }) => {
  const score = team.score;
  return (
    <Grid container>
      <Grid item xs={4}>
        <Typography variant={isCurrentPlaying ? 'h5' : 'body1'}>
          {team.name}
        </Typography>
      </Grid>
      <Grid item xs={8}>
        <Typography variant={isCurrentPlaying ? 'h5' : 'body1'} className={classes.gameScore}>
          {(!isCurrentPlaying && hasTeamNotPlayedYet(score)) ? 'yet to bat' :
            (`${score.runs}/${score.wickets}` +
             ` in ${score.oversPlayed.overNumber}.${score.oversPlayed.ballNumber}/` +
            `${totalOversInGame}`)
          }
        </Typography>
      </Grid>
    </Grid>
  );
};

TeamScore.propTypes = {
  classes: PropTypes.object.isRequired,
  team: TeamDetailsPropTypes.isRequired,
  isCurrentPlaying: PropTypes.bool.isRequired,
  totalOversInGame: PropTypes.number.isRequired,
};

const GameScore = ({ classes, gameDetails }) => {
  const battingTeam = gameDetails.teams.filter(team => team.isBatting)[0];
  const bowlingTeam = gameDetails.teams.filter(team => !team.isBatting)[0];
  return (
    <div>
      <TeamScore
        classes={classes}
        team={battingTeam}
        isCurrentPlaying
        totalOversInGame={gameDetails.totalOversInGame}
      />
      <TeamScore
        classes={classes}
        team={bowlingTeam}
        isCurrentPlaying={false}
        totalOversInGame={gameDetails.totalOversInGame}
      />
    </div>
  );
};

GameScore.propTypes = {
  classes: PropTypes.object.isRequired,
  gameDetails: GameDetailsPropTypes,
};


export const StyledGameScore = withStyles(styles)(GameScore);

export default connect(
  ({gameDetails}) => ({
    gameDetails
  })
)(StyledGameScore);
