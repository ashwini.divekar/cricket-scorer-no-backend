import React from 'react';
import { withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import Table from '@material-ui/core/Table/Table';
import TableHead from '@material-ui/core/TableHead/TableHead';
import TableRow from '@material-ui/core/TableRow/TableRow';
import TableCell from '@material-ui/core/TableCell/TableCell';
import TableBody from '@material-ui/core/TableBody/TableBody';
import BowlingStats from '../helper/BowlingStats';

const styles = {
  tableHeader: {
    marginBottom: '0',
  },
  cellStyle: {
    padding: '4px 6px 4px 6px'
  }
};

const BowlingTable = ({classes, overDetails, gameDetails}) => (
  <div>
    <h4 className={classes.tableHeader}>Bowling Table</h4>
    <Table padding="dense">
      <TableHead>
        <TableRow>
          <TableCell className={classes.cellStyle} >Bowler</TableCell>
          <TableCell className={classes.cellStyle}  numeric>Overs</TableCell>
          <TableCell className={classes.cellStyle}  numeric>Maidens</TableCell>
          <TableCell className={classes.cellStyle}  numeric>Runs</TableCell>
          <TableCell className={classes.cellStyle}  numeric>Extras</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {new BowlingStats(overDetails, gameDetails).compute().map(row => {
          return (
            <TableRow key={row.name} id={`row-${row.name.replace('*','')}`}>
              <TableCell className={classes.cellStyle}>{row.name}</TableCell>
              <TableCell className={classes.cellStyle} numeric>{row.overs}</TableCell>
              <TableCell className={classes.cellStyle} numeric>{row.maidens}</TableCell>
              <TableCell className={classes.cellStyle} numeric>{row.runs}</TableCell>
              <TableCell className={classes.cellStyle} numeric>{row.extras}</TableCell>
            </TableRow>
          );
        })}
      </TableBody>
    </Table>
  </div>
);

export const StyledBowlingTable = withStyles(styles)(BowlingTable);

export default connect(
  ({overDetails, gameDetails}) => ({overDetails: overDetails.battingTeam.overs, gameDetails}),
)(StyledBowlingTable);


