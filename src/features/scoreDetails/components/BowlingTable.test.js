import React from 'react';
import { create3Overs, createGameDetails } from '../../../testHelpers/testDataSetupHelper';
import {StyledBowlingTable} from './BowlingTable';
import { mount } from 'enzyme';

describe('Bowling Table tests', () => {
  it('should create 3 rows for 3 batsman who have played', () => {
    const props = {
      overDetails: create3Overs(),
      gameDetails: createGameDetails(),
    };

    const BowlingTable = mount(<StyledBowlingTable {...props}/>);

    // console.log(BowlingTable.debug());

    const columns = BowlingTable.find('td').map(node => node.text());
    expect(columns).toEqual([
      'PlayerB', '2', '1', '21', '0',
      'PlayerA*', '0.5' , '0', '21', '0',
    ]);
  });
});