import React from 'react';
import { withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import Table from '@material-ui/core/Table/Table';
import TableHead from '@material-ui/core/TableHead/TableHead';
import TableRow from '@material-ui/core/TableRow/TableRow';
import TableCell from '@material-ui/core/TableCell/TableCell';
import TableBody from '@material-ui/core/TableBody/TableBody';
import BattingStats from '../helper/BattingStats';

const styles = {
  tableHeader: {
    marginBottom: '0',
  },
  cellStyle: {
    padding: '4px 6px 4px 6px'
  }
};

const BattingTable = ({classes, overDetails, gameDetails}) => (
  <div>
    <h4 className={classes.tableHeader}>Batting Table</h4>
    <Table padding="dense">
      <TableHead>
        <TableRow>
          <TableCell className={classes.cellStyle} >Batsman</TableCell>
          <TableCell className={classes.cellStyle}  numeric>Runs</TableCell>
          <TableCell className={classes.cellStyle}  numeric>Balls</TableCell>
          <TableCell className={classes.cellStyle}  numeric>Fours</TableCell>
          <TableCell className={classes.cellStyle}  numeric>Sixes</TableCell>
          <TableCell className={classes.cellStyle}  numeric>S/R</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {new BattingStats(overDetails, gameDetails).compute().map(row => {
          return (
            <TableRow key={row.name} id={`row-${row.name.replace('*','')}`}>
              <TableCell className={classes.cellStyle}>{row.name}</TableCell>
              <TableCell className={classes.cellStyle} numeric>{row.runs}</TableCell>
              <TableCell className={classes.cellStyle} numeric>{row.balls}</TableCell>
              <TableCell className={classes.cellStyle} numeric>{row.fours}</TableCell>
              <TableCell className={classes.cellStyle} numeric>{row.sixes}</TableCell>
              <TableCell className={classes.cellStyle} numeric>{row.sr}</TableCell>
            </TableRow>
          );
        })}
      </TableBody>
    </Table>
  </div>
);

export const StyledBattingTable = withStyles(styles)(BattingTable);

export default connect(
  ({overDetails, gameDetails}) => ({overDetails: overDetails.battingTeam.overs, gameDetails}),
)(StyledBattingTable);


