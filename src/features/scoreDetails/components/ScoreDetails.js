import React from 'react';
import { withStyles } from '@material-ui/core';
import GameScore from './GameScore';
import PropTypes from 'prop-types';
import BattingTable from './BattingTable';
import BowlingTable from './BowlingTable';

const styles = {
  scoreDetails: {
    padding: '10px'
  }
};

const ScoreDetails = ({classes}) => (
  <div className={classes.scoreDetails}>
    <GameScore />
    <BattingTable />
    <BowlingTable />
  </div>
);

ScoreDetails.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ScoreDetails);


