import React from 'react';
import {StyledGameScore} from './GameScore';
import { mount } from 'enzyme';

describe('Game Score Tests', () => {
  let props;
  beforeEach(() => {
    props = {
      gameDetails: {
        teams: [{
          name: 'India',
          isBatting: false,
          score: {
            runs: 115,
            wickets: 10,
            oversPlayed: {
              overNumber: 19,
              ballNumber: 5
            }
          },
        },{
          name: 'Australia',
          isBatting: true,
          score: {
            runs: 120,
            wickets: 4,
            oversPlayed: {
              overNumber: 10,
              ballNumber: 2
            }
          },
        }],
        totalOversInGame: 20,
      }
    };
  });

  it('should render the team name as Australia if the 2nd team is batting', () => {
    const gameScore = mount(<StyledGameScore {...props} />);
    expect(gameScore.find('h5').length).toBeGreaterThanOrEqual(1);
    expect(gameScore.find('h5').at(0).text()).toEqual('Australia');
  });

  it('should render the team name as India if the first team is batting', () => {
    props.gameDetails.teams[0].isBatting = true;
    props.gameDetails.teams[1].isBatting = false;
    const gameScore = mount(<StyledGameScore {...props} />);
    expect(gameScore.find('h5').length).toBeGreaterThanOrEqual(1);
    expect(gameScore.find('h5').at(0).text()).toEqual('India');
  });

  it('should format batting team score as 120/4 in 10.2/20', () => {
    const gameScore = mount(<StyledGameScore {...props} />);
    expect(gameScore.find('h5').at(1).text()).toEqual('120/4 in 10.2/20');
  });

  it('should format bowling team name as India', () => {
    const gameScore = mount(<StyledGameScore {...props} />);
    expect(gameScore.find('p').at(0).text()).toEqual('India');
  });

  it('should format bowling team score as 115/10 in 19.5/20', () => {
    const gameScore = mount(<StyledGameScore {...props} />);
    expect(gameScore.find('p').at(1).text()).toEqual('115/10 in 19.5/20');
  });

  it('should show India yet to bat if no of overs and balls both 0', () => {
    props.gameDetails.teams[0].score.oversPlayed.overNumber = 0;
    props.gameDetails.teams[0].score.oversPlayed.ballNumber = 0;
    const gameScore = mount(<StyledGameScore {...props} />);
    expect(gameScore.find('p').at(0).text()).toEqual('India');
    expect(gameScore.find('p').at(1).text()).toEqual('yet to bat');
  });

  it('should show score as 0 with balls and overs as 0 for Australia as its the current batting team', () => {
    props.gameDetails.teams[1].score.oversPlayed.overNumber = 0;
    props.gameDetails.teams[1].score.oversPlayed.ballNumber = 0;
    props.gameDetails.teams[1].score.runs = 0;
    props.gameDetails.teams[1].score.wickets = 0;

    const gameScore = mount(<StyledGameScore {...props} />);

    expect(gameScore.find('h5').at(0).text()).toEqual('Australia');
    expect(gameScore.find('h5').at(1).text()).toEqual('0/0 in 0.0/20');
  })
});