import BattingStats from './BattingStats';
import { create2Overs, createGameDetails } from '../../../testHelpers/testDataSetupHelper';

describe('Batting Stats Tests', () => {

  it('should return a list of players who have batted with the current batsman marked with a *', () => {
    const overs = create2Overs();
    const battingStats = new BattingStats(overs, createGameDetails());
    expect(battingStats.compute().map(({name}) => name)).toEqual(
      ['Player1*', 'Player2*', 'Player3', 'Player4']
    );
  });

  it('should report the number of runs and balls hit by a player', () => {
    const overs = create2Overs();
    const battingStats = new BattingStats(overs, createGameDetails());
    expect(battingStats.compute().map(({name, runs, balls}) => ({name, runs, balls}))).toEqual([
      { name: 'Player1*', runs: 12, balls: 3 },
      { name: 'Player2*', runs: 9, balls: 3  },
      { name: 'Player3', runs: 9, balls: 3  },
      { name: 'Player4', runs: 18, balls: 3  },
    ]);

  });

  it('should report the number of 4s hit by a player', () => {
    const overs = create2Overs();
    const battingStats = new BattingStats(overs, createGameDetails());
    expect(battingStats.compute().map(({name, fours}) => ({name, fours}))).toEqual([
      { name: 'Player1*', fours: 3 },
      { name: 'Player2*', fours: 0 },
      { name: 'Player3', fours: 0 },
      { name: 'Player4', fours: 0 },
    ]);

  });

  it('should report the number of 6s hit by a player', () => {
    const overs = create2Overs();
    const battingStats = new BattingStats(overs, createGameDetails());
    expect(battingStats.compute().map(({name, sixes}) => ({name, sixes}))).toEqual([
      { name: 'Player1*', sixes: 0 },
      { name: 'Player2*', sixes: 0 },
      { name: 'Player3',  sixes: 0 },
      { name: 'Player4',  sixes: 3 },
    ]);
  });

  it('should report the strike rate of a player', () => {
    const overs = create2Overs();
    overs.push({
      balls: [
        { runs: 2, batsmanName: 'Player4' },
        { runs: 0, batsmanName: 'Player4' },
        { runs: 0, batsmanName: 'Player4' },
        { runs: 0, batsmanName: 'Player5' },
      ],
      bowlerName: 'Bowler',
    });
    const battingStats = new BattingStats(overs, createGameDetails());
    expect(battingStats.compute().map(({name, sr}) => ({name, sr}))).toEqual([
      { name: 'Player1*', sr: "400.00" },
      { name: 'Player2*', sr: "300.00" },
      { name: 'Player3',  sr: "300.00" },
      { name: 'Player4',  sr: "333.33" },
      { name: 'Player5',  sr: "0.00" },
    ]);
  });

});