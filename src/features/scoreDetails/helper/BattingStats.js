class BattingStats {
  constructor(overDetails, gameDetails) {
    this._overDetails = overDetails;
    this._gameDetails = gameDetails;
  }

  compute() {
    const balls = this._flatMapOversIntoBalls();
    const players = this._createPlayerStats(balls);
    return this._computeStrikeRate(
      this._markPlayersOnStrike(
        Object.values(players)
    ));
  }

  _createPlayerStats(balls) {
    return balls.reduce((accumulator, ball) => {
      const player = BattingStats._createPlayerIfNotCreated(accumulator, ball);
      BattingStats._incrementNumberOfRunsAndBalls(ball, player);
      BattingStats._checkAndIncrementNumberOfFours(ball, player);
      BattingStats._checkAndIncrementNumberOfSixes(ball, player);

      return accumulator;
    }, {});
  }

  static _incrementNumberOfRunsAndBalls(ball, player) {
    player.runs += ball.runs;
    player.balls += 1;
  }

  static _checkAndIncrementNumberOfFours(ball, player) {
    if (ball.runs === 4) {
      player.fours += 1
    }
  }

  static _checkAndIncrementNumberOfSixes(ball, player) {
    if (ball.runs === 6) {
      player.sixes += 1
    }
  }

  static _createPlayerIfNotCreated(accumulator, ball) {
    let player = accumulator[ball.batsmanName];
    if (!player) {
      player = { name: ball.batsmanName, runs: 0, balls: 0, fours: 0, sixes: 0 };
      accumulator[player.name] = player;
    }
    return player;
  }

  _computeStrikeRate(players) {
    players.forEach(player => {
      player.sr = player.balls === 0 ? 0 : ((player.runs / player.balls) * 100).toFixed(2);
    });
    return players;
  }

  _markPlayersOnStrike(players) {
    const battingPlayerNames = this._gameDetails.teams
      .find(team => team.isBatting)
      .players
        .filter(player => player.isBatting)
        .map(player => player.name);

    players
      .filter(player => battingPlayerNames.includes(player.name))
      .forEach(player => {
        player.name += "*"
      });
    return players;
  }

  _flatMapOversIntoBalls() {
    return this._overDetails.reduce((accumulator, over) => {
      return accumulator.concat(over.balls);
    }, []);
  }
}

export default BattingStats;