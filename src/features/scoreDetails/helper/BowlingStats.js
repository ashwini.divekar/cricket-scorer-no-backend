import { BYE, LEG_BYE, NO_BALL, WIDE } from '../../scorer/helper/ExtrasConstants';
import numberOfLegitimateBallsInOver from '../../scorer/helper/numberOfLegitimateBallsInOver';

class BowlingStats {
  constructor(overDetails, gameDetails) {
    this._overDetails = overDetails;
    this._gameDetails = gameDetails;
  }

  compute() {
    const bowlers = this._overDetails.reduce((accumulator, over) => {
      const player = BowlingStats._createPlayerIfNotCreated(accumulator, over);
      BowlingStats._addRunsOversMaidensAndExtras(player, over);
      return accumulator;
    }, {});

    return this._markCurrentBowler(Object.values(bowlers));
  }


  static _createPlayerIfNotCreated(accumulator, over) {
    let player = accumulator[over.bowlerName];
    if (!player) {
      player = { name: over.bowlerName, overs: 0, maidens: 0, runs: 0, extras: 0 };
      accumulator[player.name] = player;
    }
    return player;
  }

  _markCurrentBowler(players) {
    const currentBowlerName = this._gameDetails.teams
      .find(team => !team.isBatting)
      .players
        .find(player => player.isBowling)
      .name;

    const currentBowler = players
      .find(player => player.name === currentBowlerName);

    currentBowler.name += "*";

    return players;
  }

  static _addRunsOversMaidensAndExtras(player, over) {
    const noOfLegitimateBalls = numberOfLegitimateBallsInOver(over);
    if(noOfLegitimateBalls >=6) {
      player.overs += 1;
    } else {
      player.overs += noOfLegitimateBalls/10
    }

    const runsInOver = over.balls.reduce((sum, ball) => {
      if(!ball.extras.includes(BYE) && !ball.extras.includes(LEG_BYE)) {
        return sum + ball.runs;
      }
      return sum;
    }, 0);

    if(runsInOver === 0 && player.overs >= 1) {
      player.maidens += 1;
    }
    player.runs = player.runs += runsInOver;
    BowlingStats._addExtras(player, over);
  }

  static _addExtras(player, over) {
    player.extras = over.balls.reduce((extrasCount, ball) => {
      if (ball.extras.includes(WIDE) || ball.extras.includes(NO_BALL)) {
        return extrasCount + 1;
      }
      return extrasCount;
    }, 0);
  }
}

export default BowlingStats;