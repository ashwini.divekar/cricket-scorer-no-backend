import BowlingStats from './BowlingStats';
import { create3Overs, createGameDetails } from '../../../testHelpers/testDataSetupHelper';
import { BYE, LEG_BYE, NO_BALL, WIDE } from '../../scorer/helper/ExtrasConstants';

describe('Bowling Stats Tests', () => {
  let overs;
  let gameDetails;

  beforeEach(() => {
    overs = create3Overs();
    gameDetails = createGameDetails();
  });


  it('should return the bowler names with * against the current bowler', () => {
    const bowlingStats = new BowlingStats(overs, gameDetails);
    expect(bowlingStats.compute().map(({name}) => ({name}))).toEqual([
      { name: 'PlayerB' },
      { name: 'PlayerA*' },
    ])
  });

  it('should return the number of overs and runs for each bowler', () => {
    const bowlingStats = new BowlingStats(overs, gameDetails);
    expect(bowlingStats.compute().map(({name, runs, overs}) => ({name, runs, overs}))).toEqual([
      { name: 'PlayerB', overs: 2, runs: 21 },
      { name: 'PlayerA*', overs: 0.5, runs: 21 },
    ])
  });

  it('should return the number of overs not counting the extras', () => {
    overs[1].balls.push({ runs: 1, extras: [WIDE] });

    const bowlingStats = new BowlingStats(overs, gameDetails);
    expect(bowlingStats.compute().map(({name, runs, overs}) => ({name, runs, overs}))).toEqual([
      { name: 'PlayerB', overs: 2, runs: 21 },
      { name: 'PlayerA*', overs: 0.5, runs: 22 },
    ])
  });

  it('should return the increment the number of extras to 1 if there is a WIDE', () => {
    overs[1].balls.push({ runs: 1, extras: [WIDE] });

    const bowlingStats = new BowlingStats(overs, gameDetails);
    expect(bowlingStats.compute().map(({name, extras}) => ({name, extras}))).toEqual([
      { name: 'PlayerB', extras: 0 },
      { name: 'PlayerA*', extras: 1 },
    ])
  });

  it('should return the increment the number of extras to 1 if there is a NO_BALL', () => {
    overs[1].balls.push({ runs: 1, extras: [NO_BALL] });

    const bowlingStats = new BowlingStats(overs, gameDetails);
    expect(bowlingStats.compute().map(({name, extras}) => ({name, extras}))).toEqual([
      { name: 'PlayerB', extras: 0 },
      { name: 'PlayerA*', extras: 1 },
    ])
  });

  it('should return the number of runs not counting BYEs', () => {
    overs[1].balls.push({ runs: 1, extras: [BYE], batsmanName:  'Player3'});

    const bowlingStats = new BowlingStats(overs, gameDetails);
    expect(bowlingStats.compute().map(({name, runs, overs}) => ({name, runs, overs}))).toEqual([
      { name: 'PlayerB', overs: 2, runs: 21 },
      { name: 'PlayerA*', overs: 1, runs: 21 },
    ])
  });

  it('should return the number of runs not counting LEG_BYEs', () => {
    overs[1].balls.push({ runs: 1, extras: [LEG_BYE], batsmanName:  'Player3'});

    const bowlingStats = new BowlingStats(overs, gameDetails);
    expect(bowlingStats.compute().map(({name, runs, overs}) => ({name, runs, overs}))).toEqual([
      { name: 'PlayerB', overs: 2, runs: 21 },
      { name: 'PlayerA*', overs: 1, runs: 21 },
    ])
  });

  it('should return the number of maidens as 1 against PlayerB', () => {
    const bowlingStats = new BowlingStats(overs, gameDetails);
    expect(bowlingStats.compute().map(({name, maidens}) => ({name, maidens}))).toEqual([
      { name: 'PlayerB', maidens: 1 },
      { name: 'PlayerA*', maidens: 0 },
    ])
  });
});