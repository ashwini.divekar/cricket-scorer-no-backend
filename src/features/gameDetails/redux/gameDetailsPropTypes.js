import PropTypes from 'prop-types';

export const TeamDetailsPropTypes = PropTypes.shape({
  name: PropTypes.string.isRequired,
  isBatting: PropTypes.bool.isRequired,
  players: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    isBatting: PropTypes.bool,
  })),
  score: PropTypes.shape({
    runs: PropTypes.number.isRequired,
    wickets: PropTypes.number.isRequired,
    oversPlayed: PropTypes.shape({
      overNumber: PropTypes.number.isRequired,
      ballNumber: PropTypes.number.isRequired,
    }),
  })
});

export const GameDetailsPropTypes = PropTypes.shape({
  teams: PropTypes.arrayOf(TeamDetailsPropTypes),
  totalOversInGame: PropTypes.number.isRequired,
});
