import { NEXT_BALL_BUTTON_CLICKED, NEXT_INNINGS_ACTION, NEXT_OVER_ACTION } from '../../scorer/redux/currentBallReducer';
import immutable from 'object-path-immutable';
import { EXTRAS_THAT_DONT_COUNT_AS_A_BALL} from '../../scorer/helper/ExtrasConstants';
import { SELECT_PLAYER_ACTION } from '../../scorer/redux/selectPlayerModalReducer';

export const initialState = {
  teams: [{
    name: 'Team 1',
    isBatting: true,
    players: [
      { name: 'Player 1.1', isBatting: true },
      { name: 'Player 1.2', isBatting: true },
      { name: 'Player 1.3' },
      { name: 'Player 1.4' },
      { name: 'Player 1.5' },
      { name: 'Player 1.6' },
      { name: 'Player 1.7' },
      { name: 'Player 1.8' },
      { name: 'Player 1.9' },
      { name: 'Player 1.10' },
      { name: 'Player 1.11' }
    ],
    score: {
      runs: 0,
      wickets: 0,
      oversPlayed: {
        overNumber: 0,
        ballNumber: 0
      }
    }
  },
  {
    name: 'Team 2',
    isBatting: false,
    players: [
      { name: 'Player 2.1', isBowling: true },
      { name: 'Player 2.2' },
      { name: 'Player 2.3' },
      { name: 'Player 2.4' },
      { name: 'Player 2.5' },
      { name: 'Player 2.6' },
      { name: 'Player 2.7' },
      { name: 'Player 2.8' },
      { name: 'Player 2.9' },
      { name: 'Player 2.10' },
      { name: 'Player 2.11' }
    ],
    score: {
      runs: 0,
      wickets: 0,
      oversPlayed: {
        overNumber: 0,
        ballNumber: 0
      }
    }
  }],
  totalOversInGame: 2
};

const updateScore = (currentBall) => (score) => ({
    runs: score.runs + currentBall.runs,
    wickets: currentBall.isOut ? score.wickets + 1 : score.wickets,
    oversPlayed: {
      ...score.oversPlayed,
      ballNumber: currentBall.extras.find(extra =>
        EXTRAS_THAT_DONT_COUNT_AS_A_BALL.includes(extra)) ?
        score.oversPlayed.ballNumber :
        score.oversPlayed.ballNumber + 1
    }
  });

const updatePlayer = (currentBall) => player =>
    currentBall.isOut ? { ...player, isOut: true, isBatting: false } : player;

const updateOverNumber = (state) => {
  const battingTeamIndex = state.teams.findIndex(team => team.isBatting);

  return immutable.update(state, `teams.${battingTeamIndex}.score.oversPlayed`, (oversPlayed) => ({
    ballNumber: 0,
    overNumber: oversPlayed.overNumber + 1
  }));
};

export default (state = initialState, action) => {
  let battingTeamIndex;
  let playerIndex;
  switch(action.type) {
    case NEXT_BALL_BUTTON_CLICKED:
      const currentBall = action.payload.currentBall;
      battingTeamIndex = state.teams.findIndex(team => team.isBatting);
      playerIndex = state.teams[battingTeamIndex].players
        .findIndex(player => player.name === currentBall.batsmanName);

      return immutable(state)
        .update(`teams.${battingTeamIndex}.score`, updateScore(currentBall))
        .update(`teams.${battingTeamIndex}.players.${playerIndex}`, updatePlayer(currentBall))
        .value();

    case SELECT_PLAYER_ACTION:
      const teamIndex = state.teams.findIndex(team => team.isBatting === action.payload.isBattingTeam);
      playerIndex = state.teams[teamIndex].players
        .findIndex(player => player.name === action.payload.playerName);

      return immutable.update(state,
          `teams.${teamIndex}.players.${playerIndex}`,
          player => ({...player, isBatting: true}));

    case NEXT_OVER_ACTION:
      return updateOverNumber(state);

    case NEXT_INNINGS_ACTION:
      const newState = updateOverNumber(state);
      battingTeamIndex = newState.teams.findIndex(team => team.isBatting);

      return immutable(newState)
        .set(`teams.${battingTeamIndex}.isBatting`, false)
        .set(`teams.${1 - battingTeamIndex}.isBatting`, true)
        .update(`teams.${battingTeamIndex}.players`, players =>
          players.map(({name}, index) => ({name, isBowling: index === 0}))
        )
        .update(`teams.${1 - battingTeamIndex}.players`, players =>
          players.map(({name}, index) => ({ name, isBatting: index < 2 }))
        )
        .value();

    default:
      return state;
  }
};