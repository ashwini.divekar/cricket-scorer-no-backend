import gameDetailsReducer, { initialState } from './gameDetailsReducer';
import { NEXT_BALL_BUTTON_CLICKED, nextInningsAction, nextOverAction } from '../../scorer/redux/currentBallReducer';
import { NO_BALL, WIDE } from '../../scorer/helper/ExtrasConstants';
import { selectPlayerAction } from '../../scorer/redux/selectPlayerModalReducer';

describe('game details reducer tests', () => {

  const currentState = {
    teams: [
      { name: 'team1',
        isBatting: false,
        players: [
          { name: 'Player 2.1' },
          { name: 'Player 2.2', isBowling: true },
          { name: 'Player 2.3' },
          { name: 'Player 2.4' },
        ],
      },
      {
        name: 'team2',
        isBatting: true,
        players: [
          { name: 'Player 1.1', isBatting: true },
          { name: 'Player 1.2', isBatting: true },
          { name: 'Player 1.3' },
          { name: 'Player 1.4' },
        ],
        score: {
          runs: 1,
          wickets: 0,
          oversPlayed: {
            overNumber: 1,
            ballNumber: 4
          }
        }
      }
    ]
  };

  it('should return initial state if no state is provided', () => {
    const gameDetails = gameDetailsReducer(undefined, { type: 'REDUX_INIT' });
    expect(gameDetails).toBe(initialState);
  });

  it('should update the score for next ball button action', () => {
    const newState = gameDetailsReducer(currentState, {
        type: NEXT_BALL_BUTTON_CLICKED,
        payload: {
          currentBall: {
            runs: 2,
            extras: [],
          },
        }
      }
    );

    expect(newState.teams[1].score).toEqual({
      runs: 3,
      wickets: 0,
      oversPlayed: {
        overNumber: 1,
        ballNumber: 5
      }
    });
  });

  it('should not increase ball number if its a wide', () => {
    const newState = gameDetailsReducer(currentState, {
        type: NEXT_BALL_BUTTON_CLICKED,
        payload: {
          currentBall: {
            runs: 1,
            extras: [WIDE],
          },
        }
      }
    );

    expect(newState.teams[1].score).toEqual({
      runs: 2,
      wickets: 0,
      oversPlayed: {
        overNumber: 1,
        ballNumber: 4
      }
    });
  });

  it('should not increase ball number if its a no ball', () => {
    const newState = gameDetailsReducer(currentState, {
        type: NEXT_BALL_BUTTON_CLICKED,
        payload: {
          currentBall: {
            runs: 4,
            extras: [NO_BALL],
          },
        }
      }
    );

    expect(newState.teams[1].score).toEqual({
      runs: 5,
      wickets: 0,
      oversPlayed: {
        overNumber: 1,
        ballNumber: 4
      }
    });
  });

  it('should increment the number of wickets if current ball says isOut', () => {
    const newState = gameDetailsReducer(currentState, {
        type: NEXT_BALL_BUTTON_CLICKED,
        payload: {
          currentBall: {
            runs: 0,
            extras: [],
            isOut: true,
            batsmanName: 'Player 1.1',
          },
        }
      }
    );

    expect(newState.teams[1].score).toEqual({
      runs: 1,
      wickets: 1,
      oversPlayed: {
        overNumber: 1,
        ballNumber: 5
      }
    });
  });

  it('should mark a player as out if current ball says isOut', () => {
    const newState = gameDetailsReducer(currentState, {
        type: NEXT_BALL_BUTTON_CLICKED,
        payload: {
          currentBall: {
            runs: 0,
            extras: [],
            isOut: true,
            batsmanName: 'Player 1.1',
          },
        }
      }
    );

    expect(newState.teams[1].players[0]).toEqual({
      name: 'Player 1.1', isBatting: false, isOut: true,
    });
  });

  it('should update player 1.4 as batting when select player action is fired', () => {
    const newState = gameDetailsReducer(currentState, selectPlayerAction('Player 1.4', true));

    expect(newState.teams[1].players
      .find(player => player.name === 'Player 1.4').isBatting).toBeTruthy();
  });

  it('should increase the number of overs when next over action is fired', () => {
    const newState = gameDetailsReducer(currentState, nextOverAction());
    expect(newState.teams[1].score.oversPlayed).toEqual({
      ballNumber: 0,
      overNumber: 2,
    })
  });

  it('should increase the number of overs when next innings action is fired', () => {
    const newState = gameDetailsReducer(currentState, nextInningsAction());
    expect(newState.teams[1].score.oversPlayed).toEqual({
      ballNumber: 0,
      overNumber: 2,
    })
  });

  it('should switch the batting team indexes ', () => {
    const newState = gameDetailsReducer(currentState, nextInningsAction());
    expect(newState.teams[0].isBatting).toBeTruthy();
    expect(newState.teams[1].isBatting).toBeFalsy();
  });

  it('should make the team2 batsmen as not batting', () => {
    const newState = gameDetailsReducer(currentState, nextInningsAction());
    expect(newState.teams[1].players.filter(player => player.isBatting).length).toEqual(0);
  });

  it('should make the the first 2 players of team1 as batsmen', () => {
    const newState = gameDetailsReducer(currentState, nextInningsAction());
    expect(newState.teams[0].players[0].isBatting).toBeTruthy();
    expect(newState.teams[0].players[1].isBatting).toBeTruthy();
  });

  it('should make the first player of team1 the bowler', () => {
    const newState = gameDetailsReducer(currentState, nextInningsAction());
    expect(newState.teams[1].players[0].isBowling).toBeTruthy();
  });

});