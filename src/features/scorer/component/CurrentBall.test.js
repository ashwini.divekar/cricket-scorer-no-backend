import React from 'react';
import {mount} from 'enzyme';
import {StyledCurrentBall} from './CurrentBall';
import { NO_BALL, WIDE } from '../helper/ExtrasConstants';

describe('current ball tests', () => {
  const props = {
    gameDetails: {
      teams: [{
        name: 'Team 1',
        isBatting: true,
        players: [
          { name: 'Sachin', isBatting: true },
          { name: 'Sourav', isBatting: true },
          { name: 'Sehwag' },
          { name: 'Srinath' }
        ]
      },
      {
        name: 'Team 2',
        isBatting: false,
        players: [
          { name: 'Player B.1' },
          { name: 'Player B.2' },
        ]
      }],
      totalOversInGame: 2,
    },
    currentBall: { extras: [] },
    batsmanButtonClicked: jest.fn(),

  };

  it('should display names of 1st 2 batsman in batting team', () => {
    const currentBall = mount(<StyledCurrentBall {...props} />);
    expect(currentBall.find('#batsman0').at(0).text()).toEqual('Sachin');
    expect(currentBall.find('#batsman1').at(0).text()).toEqual('Sourav');
  });

  it('should highlight Sachin in primary colour when current ball state has batsman as Sachin', () => {
    const testProps = {
      ...props,
      currentBall: {
        extras: [],
        batsmanName: 'Sachin'
      }
    };
    const currentBall = mount(<StyledCurrentBall {...testProps} />);
    expect(currentBall.find('#batsman0').at(0).prop('color')).toEqual('primary');
  });

  it('should highlight 4 in primary colour when button 4 is selected', () => {
    const testProps = {
      ...props,
      currentBall: {
        runs: 4,
        extras: [],
      }
    };
    const currentBall = mount(<StyledCurrentBall {...testProps} />);
    expect(currentBall.find('#button4').at(0).prop('color')).toEqual('primary');
  });

  it('should dispatch the runButtonAction', () => {
    const testProps = {
      ...props,
      currentBall: { extras: [] },
      runButtonClicked: jest.fn(),
    };

    const currentBall = mount(<StyledCurrentBall {...testProps}/>);
    currentBall.find('#button1').at(0).simulate('click');

    expect(testProps.runButtonClicked.mock.calls[0][0]).toEqual(1);
  });

  it('should dispatch the batsmanButtonAction', () => {
    const testProps = {
      ...props,
      currentBall: { extras: [] },
      batsmanButtonClicked: jest.fn(),
    };

    const currentBall = mount(<StyledCurrentBall {...testProps}/>);
    currentBall.find('#batsman0').at(0).simulate('click');

    expect(testProps.batsmanButtonClicked.mock.calls[0][0]).toEqual('Sachin');
  });

  it('should highlight Wide as primary and No Ball as default when the currentBall has only a wide in it', () => {
    const testProps = {
      ...props,
      currentBall: { extras: [WIDE] },
      batsmanButtonClicked: jest.fn(),
    };
    const currentBall = mount(<StyledCurrentBall {...testProps}/>);
    expect(currentBall.find(`#button${WIDE}`).at(0).prop('color')).toEqual('primary');
    expect(currentBall.find(`#button${NO_BALL}`).at(0).prop('color')).toEqual('default');
  });

  it('should call the toggleExtraButtonAction when the extra button is clicked', () => {
    const testProps = {
      ...props,
      currentBall: { extras: [WIDE] },
      batsmanButtonClicked: jest.fn(),
      toggleExtraButtonAction: jest.fn(),
    };

    const currentBall = mount(<StyledCurrentBall {...testProps}/>);
    currentBall.find(`#button${WIDE}`).at(0).simulate('click');
    expect(testProps.toggleExtraButtonAction.mock.calls[0][0]).toEqual(WIDE);
  });
});