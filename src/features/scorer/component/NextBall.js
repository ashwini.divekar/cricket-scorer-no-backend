import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core';
import { connect } from 'react-redux';
import { nextBallAction } from '../redux/currentBallReducer';
import { CurrentBallPropTypes } from '../redux/currentBallPropTypes';

const styles = {
  nextBall: {
    marginTop: '20px',
  }
};

const NextBall = ({classes, currentBall, nextBallClicked}) => (
  <Grid container justify="center" className={classes.nextBall}>
    <Grid item xs={6}>
      <Button variant="contained" fullWidth disabled={currentBall.runs === undefined}
              onClick={() => nextBallClicked(currentBall)}>
        Next Ball
      </Button>
    </Grid>
  </Grid>
);

NextBall.propTypes = {
  classes: PropTypes.object.isRequired,
  currentBall: CurrentBallPropTypes.isRequired,
  nextBallClicked: PropTypes.func.isRequired,
};

export const StyledNextBall = withStyles(styles)(NextBall);

export default connect(
  ({currentBall}) => ({currentBall}),
  (dispatch) => ({
    nextBallClicked: (currentBall) => dispatch(nextBallAction(currentBall))
  })
)(StyledNextBall);