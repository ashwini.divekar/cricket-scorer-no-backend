import React from 'react';
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core';
import GameScore from '../../scoreDetails/components/GameScore';
import CurrentOver from './CurrentOver';
import CurrentBall from './CurrentBall';
import NextBall from './NextBall';
import BatsmenOut from './BatsmenOut';
import ChooseBatsman from './ChoosePlayer';
import Button from '@material-ui/core/Button/Button';

const styles = {
  scorer: {
    padding: '10px'
  },
  scoreDetailsButton: {
    margin: '10px auto 0 auto',
    display: 'block',
    textAlign: 'center',
    width: '150px',
    fontWeight: 'bold',
  }
};

const Scorer = ({classes}) => (
  <div className={classes.scorer}>
    <GameScore />
    <Button component={Link} to="/scoreDetails" color='primary' className={classes.scoreDetailsButton}>
      Score Details
    </Button>
    <CurrentOver/>
    <CurrentBall/>
    <BatsmenOut/>
    <NextBall />
    <ChooseBatsman />
  </div>
);

export default withStyles(styles)(Scorer);
