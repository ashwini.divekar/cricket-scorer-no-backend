import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import { withStyles } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import { GameDetailsPropTypes } from '../../gameDetails/redux/gameDetailsPropTypes';
import { selectPlayerAction } from '../redux/selectPlayerModalReducer';
import { OverDetailsPropTypes } from '../redux/overDetailsPropTypes';

const styles = {
  chooseBatsman: {
    margin: '10px',
  }
};

const filterBatsmen = (gameDetails) =>
  gameDetails.teams
    .find(team => team.isBatting)
    .players
      .filter(player => !player.isOut && !player.isBatting)
      .map(player => player.name);

const filterBowlers = (gameDetails, overDetails) =>
  gameDetails.teams
    .find(team => !team.isBatting)
    .players
      .filter(player => player.name !== overDetails.bowlerName)
      .map(player => player.name);

const filterPlayerList = (gameDetails, selectPlayerModal, overDetails) =>
  selectPlayerModal.showBattingTeamPlayers ?
    filterBatsmen(gameDetails) :
    filterBowlers(gameDetails, overDetails);


const ChoosePlayer = ({classes, selectPlayerModal, gameDetails, selectPlayerAction, overDetails}) => (
  <Dialog
    className={classes.chooseBatsman}
    open={selectPlayerModal.showModal}
    aria-labelledby="choose-player"
  >
    <DialogTitle id="choose-player">
      Next {selectPlayerModal.showBattingTeamPlayers ? 'Batsman' : 'Bowler'}
    </DialogTitle>
    <FormControl className={classes.chooseBatsman}>
      <NativeSelect
        name="nextPlayer"
        onChange={(event) =>
          selectPlayerAction(
            event.target.value,
            selectPlayerModal.showBattingTeamPlayers)}
      >
        {["Select..."].concat(filterPlayerList(gameDetails, selectPlayerModal, overDetails))
          .map(playerName =>
            <option key={playerName} value={playerName}>{playerName}</option>)}
      </NativeSelect>
    </FormControl>
  </Dialog>
);

ChoosePlayer.propTypes = {
  classes: PropTypes.object.isRequired,
  selectPlayerModal: PropTypes.shape({
    showModal: PropTypes.bool.isRequired,
    showBattingTeamPlayers: PropTypes.bool.isRequired,
  }),
  gameDetails: GameDetailsPropTypes,
  selectPlayerAction: PropTypes.func.isRequired,
  overDetails: OverDetailsPropTypes.isRequired,
};

export const StyledChooseBatsman = withStyles(styles)(ChoosePlayer);

export default connect(
  ({selectPlayerModal, gameDetails, overDetails}) => ({
    selectPlayerModal,
    gameDetails,
    overDetails: overDetails.battingTeam.overs[overDetails.battingTeam.overs.length - 1],
  }),
  (dispatch) => ({
    selectPlayerAction: (playerName, isBattingTeam) =>
      dispatch(selectPlayerAction(playerName, isBattingTeam))
  })
)(StyledChooseBatsman);


