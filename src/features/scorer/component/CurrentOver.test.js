import React from 'react';
import { StyledCurrentOver } from './CurrentOver';
import { mount } from 'enzyme';
import { WIDE } from '../helper/ExtrasConstants';

describe('current over tests', () => {
  it('should show  empty if no balls played', () => {
    const props = {
      currentOver: {
        balls: []
      }
    };
    const currentOver = mount(<StyledCurrentOver {...props} />);
    expect(currentOver.find('#currentOverDetails').at(0).text()).toEqual('');
  });

  it('should show 1 1Wd', () => {
    const props = {
      currentOver: {
        balls: [{
          runs: 1,
        }, {
          runs: 1,
          extras: [WIDE],
        }]
      }
    };
    const currentOver = mount(<StyledCurrentOver {...props} />);
    expect(currentOver.find('#currentOverDetails').at(0).text()).toEqual('1 1Wd');
  });

  it('should show the bowler name as Srinath', () => {
    const props = {
      currentOver: {
        balls: [],
        bowlerName: 'Srinath'
      }
    };
    const currentOver = mount(<StyledCurrentOver {...props} />);
    expect(currentOver.find('#bowlerDetails').at(0).text()).toEqual('Bowler: Srinath');
  });
});