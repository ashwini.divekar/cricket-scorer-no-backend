import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core';
import {connect} from 'react-redux';
import Grid from '@material-ui/core/Grid/Grid';
import Button from '@material-ui/core/Button/Button';
import { batsmanButtonAction, runButtonAction, toggleExtraButtonAction } from '../redux/currentBallReducer';
import { EXTRA_REPRESENTATION } from '../helper/ExtrasConstants';
import {CurrentBallPropTypes} from '../redux/currentBallPropTypes';
import { GameDetailsPropTypes } from '../../gameDetails/redux/gameDetailsPropTypes';

const styles = {
  currentBallHeader: {
    marginBottom: '10px'
  },
  runs: {
    textAlign: 'center',
  },
  runsButton: {
    width: '30px',
    margin: '10px 5px',
  },
  extrasButtonsContainer: {
    textAlign: 'center',
  },
  extrasButton: {
    width: '30px',
    margin: '0px 5px',
  }
};

const CurrentBallHeader = (props) => {
  return (
    <div>
      <Grid container className={props.classes.currentBallHeader}>
        <Grid item xs={12}>
          <h4>This Ball</h4>
        </Grid>
      </Grid>
      <Grid container justify="center" className={props.classes.currentBallHeader}>
        <BatsmanButton number={0} {...props} />
        <BatsmanButton number={1} {...props} />
      </Grid>
    </div>
  );
};

CurrentBallHeader.propTypes = {
  classes: PropTypes.object.isRequired,
  currentBall: CurrentBallPropTypes.isRequired,
  gameDetails: GameDetailsPropTypes.isRequired,
  batsmanButtonClicked: PropTypes.func.isRequired,
};

const BatsmanButton = ({number, currentBall, gameDetails, batsmanButtonClicked}) => {
  const battingTeam = gameDetails.teams.filter(team => team.isBatting)[0];
  const playersBatting = battingTeam.players.filter(player => player.isBatting);
  const batsmanName = playersBatting[number] ? playersBatting[number].name : 'Choose Player';

  return (
    <Grid item xs={4}>
      <Button id={`batsman${number}`} variant="contained" fullWidth
              color={currentBall && currentBall.batsmanName === batsmanName ? 'primary' : 'default'}
              onClick={() => batsmanButtonClicked(batsmanName)}>
        {batsmanName}
      </Button>
    </Grid>
  );
};

BatsmanButton.propTypes = {
  classes: PropTypes.object.isRequired,
  number: PropTypes.number.isRequired,
  currentBall: CurrentBallPropTypes.isRequired,
  gameDetails: GameDetailsPropTypes.isRequired,
  batsmanButtonClicked: PropTypes.func.isRequired,
};

const RunsButtons = ({classes, currentBall, runButtonClicked}) => (
  <div className={classes.runs}>
    {
      [0, 1, 2, 3, 4, 5, 6, 7].map(number => (
        <Button id={`button${number}`} key={number}
                color={currentBall && currentBall.runs === number ? 'primary' : 'default'}
                variant="contained" className={classes.runsButton}
                onClick={() => runButtonClicked(number)}>{number}</Button>
      ))
    }
  </div>
);

RunsButtons.propTypes = {
  classes: PropTypes.object.isRequired,
  currentBall: CurrentBallPropTypes.isRequired,
};

const Extras = ({ classes, currentBall, toggleExtraButtonAction }) => <div>
  <div>
    <p>Extras</p>
    <div className={classes.extrasButtonsContainer}>
      {Object.keys(EXTRA_REPRESENTATION).map(extra => (
        <Button
          id={`button${extra}`}
          key={extra}
          variant="contained"
          className={classes.extrasButton}
          color={currentBall.extras.includes(extra) ? 'primary' : 'default'}
          onClick={() => toggleExtraButtonAction(extra)}
        >
          {EXTRA_REPRESENTATION[extra]}
        </Button>
      ))}
    </div>
  </div>
</div>;

RunsButtons.propTypes = {
  classes: PropTypes.object.isRequired,
  currentBall: CurrentBallPropTypes.isRequired,
};

const CurrentBall = (props) => (
  <div>
    <CurrentBallHeader {...props} />
    <RunsButtons {...props}/>
    <Extras {...props}/>
  </div>
);

export const StyledCurrentBall = withStyles(styles)(CurrentBall);

export default connect(
  ({currentBall, gameDetails}) => ({currentBall: currentBall || {}, gameDetails}),
  (dispatch) => ({
    runButtonClicked: (number) => dispatch(runButtonAction(number)),
    batsmanButtonClicked: (name) => dispatch(batsmanButtonAction(name)),
    toggleExtraButtonAction: (extra) => dispatch(toggleExtraButtonAction(extra)),
  })
)(StyledCurrentBall);




