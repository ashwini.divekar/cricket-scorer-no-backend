import React from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { toggleOutButtonAction } from '../redux/currentBallReducer';
import PropTypes from 'prop-types';
import { CurrentBallPropTypes } from '../redux/currentBallPropTypes';

const styles = {
  batsmanOut: {
    marginTop: '20px',
  }
};

const BatsmenOut = ({classes, currentBall, outBallClicked}) => (
  <div>
    <Button
      className={classes.batsmanOut}
      variant="contained"
      color={currentBall.isOut ? 'primary' : 'default'}
      onClick={outBallClicked}
    >
      Out
    </Button>
  </div>
);

BatsmenOut.propTypes = {
  classes: PropTypes.object.isRequired,
  currentBall: CurrentBallPropTypes.isRequired,
  outBallClicked: PropTypes.func.isRequired,
};

export const StyledBatsmenOut = withStyles(styles)(BatsmenOut);

export default connect(
  ({currentBall}) => ({currentBall}),
  (dispatch) => ({
    outBallClicked: () => dispatch(toggleOutButtonAction()),
  })
)(StyledBatsmenOut);


