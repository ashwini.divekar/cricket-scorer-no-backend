import React from 'react';
import {StyledNextBall} from './NextBall';
import {mount} from 'enzyme';
import Button from '@material-ui/core/Button';

describe('NextBall tests', () => {
  it('should fire next ball action with the current ball as an arg when next ball is clicked', () => {
    const props = {
      nextBallClicked: jest.fn(),
      currentBall: { runs: 2 },
    };
    const nextBall = mount(<StyledNextBall {...props}/>);
    const nextBallButton = nextBall.find(Button);
    nextBallButton.simulate('click');

    expect(props.nextBallClicked.mock.calls[0][0]).toEqual(props.currentBall);
  });

  it('nextBall button should be disabled when runs are not selected', () => {
    const props = {
      nextBallClicked: jest.fn(),
      currentBall: { extras: [] },
    };
    const nextBall = mount(<StyledNextBall {...props}/>);
    const nextBallButton = nextBall.find(Button);
    expect(nextBallButton.prop('disabled')).toBeTruthy();
  });

  it('nextBall button should be enabled when runs are  selected', () => {
    const props = {
      nextBallClicked: jest.fn(),
      currentBall: { runs: 0, extras: [] },
    };
    const nextBall = mount(<StyledNextBall {...props}/>);
    const nextBallButton = nextBall.find(Button);
    expect(nextBallButton.prop('disabled')).toBeFalsy();
  });
});