import React from 'react';
import { withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid/Grid';
import ballRepresentation from '../helper/ballRepresentation';

const styles = {
  currentOver: {
    marginTop: '10px',
  },
  currentOverHeading: {
    fontWeight: 'bold',
  },
  currentOverDetails: {
    paddingLeft: '20px',
  }
};

const CurrentOver = ({ classes, currentOver }) => (
  <Grid container className={classes.currentOver}>
    <Grid item xs={12}>
      <p>
        <span className={classes.currentOverHeading}>This Over</span>
        <span id='currentOverDetails' className={classes.currentOverDetails}>
          {ballRepresentation(currentOver.balls)}
        </span>
        <br/>
        <span id="bowlerDetails">Bowler: {currentOver.bowlerName}</span>
      </p>
    </Grid>
  </Grid>
);

export const StyledCurrentOver = withStyles(styles)(CurrentOver);

export default connect(
  ({ overDetails }) => ({
    currentOver: overDetails.battingTeam.overs.length === 0 ? { balls: [], bowlerName: ''}
      : overDetails.battingTeam.overs[overDetails.battingTeam.overs.length - 1]
  }),
)(StyledCurrentOver);


