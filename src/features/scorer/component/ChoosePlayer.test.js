import React from 'react';
import { mount } from 'enzyme';
import Dialog from '@material-ui/core/Dialog/Dialog';
import { StyledChooseBatsman } from './ChoosePlayer';
import NativeSelect from '@material-ui/core/NativeSelect/NativeSelect';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';

describe('Choose Batsman tests', () => {
  const props = {
    selectPlayerModal: {
      showModal: true,
      showBattingTeamPlayers: true,
    },
    gameDetails: {
      teams: [{
        name: 'Team 1',
        isBatting: true,
        players: [
          { name: 'Player 1.1', isBatting: false, isOut: true },
          { name: 'Player 1.2', isBatting: true },
          { name: 'Player 1.3' },
          { name: 'Player 1.4' },
        ],
      },
      {
        name: 'Team 2',
        isBatting: false,
        players: [
          { name: 'Player 2.1' },
          { name: 'Player 2.2' },
          { name: 'Player 2.3' },
        ],
      }],
      totalOversInGame: 2,
    },
    overDetails: {
      balls: [],
      bowlerName: 'Player 2.1',
    },
    selectPlayerAction: jest.fn(),
  };

  it('should open if modal reducer has showModal as true', () => {
    const BatsmenOut = mount(<StyledChooseBatsman {...props}/>);
    const dialog = BatsmenOut.find(Dialog);

    expect(dialog.prop('open')).toBeTruthy();
  });

  it('should open if modal reducer has showModal as false', () => {
    const changedProps = { ...props,
      selectPlayerModal: {
        showModal: false,
        showBattingTeamPlayers: false,
      }
    };

    const BatsmenOut = mount(<StyledChooseBatsman {...changedProps}/>);
    const dialog = BatsmenOut.find(Dialog);

    expect(dialog.prop('open')).toBeFalsy();
  });

  it('should display list of not out batsman', () => {
    const BatsmenOut = mount(<StyledChooseBatsman {...props}/>);
    const options = BatsmenOut.find('option');

    expect(options.map(child => child.text())).toEqual(['Select...', 'Player 1.3', 'Player 1.4'])
  });

  it('should call the selectPlayerAction with Batting Team and Player Name', () => {
    const BatsmenOut = mount(<StyledChooseBatsman {...props}/>);
    const select = BatsmenOut.find(NativeSelect);
    select.at(0).props().onChange({target: {value: 'Player 1.4'}});

    expect(props.selectPlayerAction.mock.calls.length).toEqual(1);
    expect(props.selectPlayerAction.mock.calls[0][0]).toEqual('Player 1.4');
    expect(props.selectPlayerAction.mock.calls[0][1]).toBeTruthy();
  });

  it('should show title as Next Batsman if the showBattingTeamPlayers is true', () => {
    const modal = mount(<StyledChooseBatsman {...props}/>);
    expect(modal.find(DialogTitle).text()).toEqual('Next Batsman');
  });

  it('should show title as Next Bowler if the showBattingTeamPlayers is false', () => {
    const bowlerProps = {...props, selectPlayerModal: {
      ...props.selectPlayerModal,
      showBattingTeamPlayers: false,
    }};

    const modal = mount(<StyledChooseBatsman {...bowlerProps}/>);
    expect(modal.find(DialogTitle).text()).toEqual('Next Bowler');
  });

  it('should display list of players who bowled the last over', () => {
    const bowlerProps = {...props, selectPlayerModal: {
        ...props.selectPlayerModal,
        showBattingTeamPlayers: false,
      }};

    const Players = mount(<StyledChooseBatsman {...bowlerProps}/>);
    const options = Players.find('option');

    expect(options.map(child => child.text())).toEqual(['Select...', 'Player 2.2', 'Player 2.3'])
  });


});