import React from 'react';
import {mount} from 'enzyme';
import Button from '@material-ui/core/Button';
import {StyledBatsmenOut} from './BatsmenOut';

describe('Batsman Out tests', () => {
  it('should fire out ball action when out ball is clicked', () => {
    const props = {
      outBallClicked: jest.fn(),
      currentBall: { runs: 2 },
    };
    const out = mount(<StyledBatsmenOut {...props}/>);
    const outButton = out.find(Button);
    outButton.simulate('click');

    expect(props.outBallClicked.mock.calls.length).toEqual(1);
  });

  it('batsman out button should appear selected when isOut is true', () => {
    const props = {
      outBallClicked: jest.fn(),
      currentBall: { isOut: true },
    };
    const out = mount(<StyledBatsmenOut {...props}/>);
    const outButton = out.find(Button);
    expect(outButton.prop('color')).toEqual('primary');
  });

  it('batsman out button should be default when isOut is false', () => {
    const props = {
      outBallClicked: jest.fn(),
      currentBall: { isOut: false },
    };
    const out = mount(<StyledBatsmenOut {...props}/>);
    const outButton = out.find(Button);
    expect(outButton.prop('color')).toEqual('default');
  });

});