import { nextBallAction, nextInningsAction, nextOverAction } from './currentBallReducer';
import { checkNextBallMiddleware } from './checkNextBallMiddleware';
import { NO_BALL, WIDE } from '../helper/ExtrasConstants';

describe('check next ball middleware tests', () => {

  const fakeStore = {};
  const action = nextBallAction({
    currentBall: {
      runs: 1,
    }
  });

  it('should dispatch next ball action first then a nextOverAction when the number of ball is 6 without extras', () => {
    const fakeNext = jest.fn();
    const balls = Array(6).fill({extras: []});
    fakeStore.getState = jest.fn().mockReturnValue({
      overDetails: {
        battingTeam: {
          overs: [{
            balls: balls
          }]
        }
      },
      gameDetails: {
        totalOversInGame: 2
      }
    });

    checkNextBallMiddleware(fakeStore)(fakeNext)(action);

    expect(fakeNext.mock.calls.length).toEqual(2);
    expect(fakeNext.mock.calls[0][0]).toEqual(action);
    expect(fakeNext.mock.calls[1][0]).toEqual(nextOverAction());
  });

  it('should not dispatch next over action when the over is not done yet', () => {
    const fakeNext = jest.fn();
    const balls = Array(5).fill({extras: []});
    fakeStore.getState = jest.fn().mockReturnValue({
      overDetails: {
        battingTeam: {
          overs: [{
            balls: balls
          }]
        }
      },
      gameDetails: {
        totalOversInGame: 2
      }
    });

    checkNextBallMiddleware(fakeStore)(fakeNext)(action);

    expect(fakeNext.mock.calls.length).toEqual(1);
    expect(fakeNext.mock.calls[0][0]).toEqual(action);
  });

  it('should not dispatch next over action when extras are present and the over is not done yet', () => {
    const fakeNext = jest.fn();
    fakeStore.getState = jest.fn().mockReturnValue({
      overDetails: {
        battingTeam: {
          overs: [{
            balls: [{extras: []},
                    {extras: [WIDE]},
                    {extras: [NO_BALL]},
                    {extras: []},
                    {extras: []},
                    {extras: []},]
          }]
        }
      },
      gameDetails: {
        totalOversInGame: 2
      }
    });

    checkNextBallMiddleware(fakeStore)(fakeNext)(action);

    expect(fakeNext.mock.calls.length).toEqual(1);
    expect(fakeNext.mock.calls[0][0]).toEqual(action);
  });

  it('should dispatch next over action when extras are present and the over is done', () => {
    const fakeNext = jest.fn();
    fakeStore.getState = jest.fn().mockReturnValue({
      overDetails: {
        battingTeam: {
          overs: [{
            balls: [{extras: []},
                    {extras: [WIDE]},
                    {extras: [NO_BALL]},
                    {extras: []},
                    {extras: []},
                    {extras: []},
                    {extras: []},
                    {extras: []},]
          }]
        }
      },
      gameDetails: {
        totalOversInGame: 2
      }
    });

    checkNextBallMiddleware(fakeStore)(fakeNext)(action);

    expect(fakeNext.mock.calls.length).toEqual(2);
    expect(fakeNext.mock.calls[0][0]).toEqual(action);
    expect(fakeNext.mock.calls[1][0]).toEqual(nextOverAction());
  });


  it('should dispatch next innings action when the required number of overs are up', () => {
    const fakeNext = jest.fn();
    fakeStore.getState = jest.fn().mockReturnValue({
      overDetails: {
        battingTeam: {
          overs: Array(2).fill({ balls: Array(6).fill({runs: 0, extras: []}), bowlerName: 'Player'})
        }
      },
      gameDetails: {
        totalOversInGame: 2
      }
    });

    checkNextBallMiddleware(fakeStore)(fakeNext)(action);

    expect(fakeNext.mock.calls.length).toEqual(2);
    expect(fakeNext.mock.calls[0][0]).toEqual(action);
    expect(fakeNext.mock.calls[1][0]).toEqual(nextInningsAction());
  });
});