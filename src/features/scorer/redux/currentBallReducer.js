import immutable from 'object-path-immutable';

export const RUNS_BUTTON_CLICKED = 'Scorer/RUNS_BUTTON_CLICKED';
export const EXTRAS_BUTTON_CLICKED = 'Scorer/EXTRA_BUTTON_CLICKED';
export const OUT_BUTTON_CLICKED = 'Scorer/OUT_BUTTON_CLICKED';
export const BATSMAN_BUTTON_CLICKED = 'Scorer/BATSMAN_BUTTON_CLICKED';
export const NEXT_BALL_BUTTON_CLICKED = 'Scorer/NEXT_BALL_BUTTON_CLICKED';
export const NEXT_OVER_ACTION = 'Scorer/NEXT_OVER_ACTION';
export const NEXT_INNINGS_ACTION = 'Scorer/NEXT_INNINGS_ACTION';

export const runButtonAction = (runs) => ({
  type: RUNS_BUTTON_CLICKED,
  payload: {
    runs
  }
});

export const toggleExtraButtonAction = (extra) => ({
  type: EXTRAS_BUTTON_CLICKED,
  payload: {
    extra
  },
});

export const toggleOutButtonAction = () => ({
  type: OUT_BUTTON_CLICKED,
});

export const batsmanButtonAction = (name) => ({
  type: BATSMAN_BUTTON_CLICKED,
  payload: {
    name
  }
});

/* the below function will not work properly:
 when nextBallAction is fired, only then does the overDetails have the latest ball added
 to its array.
 in order to make this work:
 Option 1: We need to examine the over details without the last ball added
 and check independently if the last ball is a legitimate ball.
 Option 2: Else we will have to add the current ball into the over details and then check. But this will be
 reproducing what the overDetails reducer is doing over here as well.
 Option 3: We need to create a middleware.
*/
// export const dispatchNextBallAction = (dispatch) => (currentBall, overDetails) => {
//     dispatch(nextBallAction(currentBall));
//     if(doesOverHave6LegitimateBalls(overDetails)) {
//       dispatch(nextOverAction());
//     }
// };

export const nextBallAction = (currentBall) => ({
  type: NEXT_BALL_BUTTON_CLICKED,
  payload: {
    currentBall
  }
});

export const nextOverAction = () => ({
  type: NEXT_OVER_ACTION,
});

export const nextInningsAction = () => ({
  type: NEXT_INNINGS_ACTION,
});

export const initialState = { extras: [], isOut: false };

export default (state = initialState, action) => {
  switch (action.type) {
    case RUNS_BUTTON_CLICKED:
      return { ...state, runs: action.payload.runs };
    case BATSMAN_BUTTON_CLICKED:
      return { ...state, batsmanName: action.payload.name };
    case NEXT_BALL_BUTTON_CLICKED:
      return initialState;
    case EXTRAS_BUTTON_CLICKED:
      return immutable.update(state, 'extras', (extras = []) => {
        if(extras.includes(action.payload.extra)) {
          return extras.filter(extra => extra !== action.payload.extra);
        }
        return [...extras, action.payload.extra]
      });
    case OUT_BUTTON_CLICKED:
      return { ...state, isOut: !state.isOut };
   default:
      return state;
  }
};