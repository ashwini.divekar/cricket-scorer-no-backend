import PropTypes from 'prop-types';

export const CurrentBallPropTypes = PropTypes.shape({
  runs: PropTypes.number,
  batsmanName: PropTypes.string,
  extras: PropTypes.arrayOf(PropTypes.string),
});

