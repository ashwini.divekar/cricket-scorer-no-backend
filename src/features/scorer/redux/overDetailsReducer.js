import immutable from 'object-path-immutable';
import { NEXT_BALL_BUTTON_CLICKED, NEXT_INNINGS_ACTION } from './currentBallReducer';
import { SELECT_PLAYER_ACTION } from './selectPlayerModalReducer';

export const initialState = {
  battingTeam: {
    overs: [{
      balls: [],
      bowlerName: 'Player 2.1',
    }]
  },
  bowlingTeam: {
    overs: [{
      balls: [],
      bowlerName: 'Player 1.1',
    }]
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case NEXT_BALL_BUTTON_CLICKED:
      const currentBall = action.payload.currentBall;
      const numberOfOversPlayed = state.battingTeam.overs.length;
      // Instead of doing this complicated immutable state copies,
      // using the immutable library which makes code look more saner.
      // return {
      //   ...state,
      //   battingTeam: {
      //     ...state.battingTeam,
      //     overs: [
      //       ...state.battingTeam.overs.slice(0, numberOfOversPlayed - 2),
      //       {
      //         ...state.battingTeam.overs[numberOfOversPlayed - 1],
      //         balls: [
      //           ...state.battingTeam.overs[numberOfOversPlayed - 1].balls,
      //           {...currentBall}
      //         ]
      //       }
      //     ]
      //   }
      // };
      return immutable.push(state,
        `battingTeam.overs.${numberOfOversPlayed - 1}.balls`,
        {...currentBall});

    case SELECT_PLAYER_ACTION:
      return action.payload.isBattingTeam ? state :
        immutable.push(state, 'battingTeam.overs', {
          balls: [],
          bowlerName: action.payload.playerName
        });
    case NEXT_INNINGS_ACTION:
      return {
        battingTeam: {...state.bowlingTeam},
        bowlingTeam: {...state.battingTeam}
      };
     default:
      return state;
  }
}