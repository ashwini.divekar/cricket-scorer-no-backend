import selectPlayerModalReducer, { initialState, selectPlayerAction } from './selectPlayerModalReducer';
import { nextBallAction, nextOverAction } from './currentBallReducer';

describe('select player modal tests', () => {
  it('should should return the initial state when no state is provided', () => {
    expect(selectPlayerModalReducer(undefined, { type: 'REDUX_INIT' })).toEqual(initialState);
  });

  it('should set showModal to true and showBatting team to true when next ball action is fired with out set', () => {
    expect(selectPlayerModalReducer(undefined, nextBallAction({isOut: true}))).toEqual({
      showModal: true,
      showBattingTeamPlayers: true,
    });
  });

  it('should not set showModal to true if no one is out', () => {
    expect(selectPlayerModalReducer(undefined, nextBallAction({isOut: false}))).toEqual({
      showModal: false,
      showBattingTeamPlayers: false,
    });
  });

  it('should close the modal when player is selected', () => {
    const currentState = { showModal: true, showBattingTeamPlayers: true};
    expect(selectPlayerModalReducer(currentState, selectPlayerAction('Ashwini'))).toEqual(initialState);
  });

  it('should set showModal to true and showBatting team to false when next over action is fired', () => {
    expect(selectPlayerModalReducer(undefined, nextOverAction())).toEqual({
      showModal: true,
      showBattingTeamPlayers: false,
    });
  });


});