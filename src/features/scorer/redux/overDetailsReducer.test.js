import overDetailsReducer, { initialState as reduxInitialState } from './overDetailsReducer';
import { NEXT_BALL_BUTTON_CLICKED } from './currentBallReducer';
import { selectPlayerAction } from './selectPlayerModalReducer';

describe('over details reducer tests', () => {

  const initialState = {
    battingTeam: {
      overs: [{
        bowlerName: 'Sharma',
      }]
    }
  };

  it('should return the initial state when no state is provided', () => {
    const state = overDetailsReducer(undefined, { type: 'REDUX_INIT' });
    expect(state).toEqual(reduxInitialState);
  });

  it('should set the current ball details in batting team overs for 1st Ball', () => {
    const state = overDetailsReducer(initialState, {
      type: NEXT_BALL_BUTTON_CLICKED,
      payload: {
        currentBall: {
          runs: 4,
          batsmanName: 'Kohli'
        }
      }
    });

    const expectedState = {
      battingTeam: {
        overs: [{
          balls: [{
            runs: 4,
            batsmanName: 'Kohli'
          }],
          bowlerName: 'Sharma'
        }]
      }
    };
    expect(state.battingTeam).toEqual(expectedState.battingTeam);
  });

  it('should set the current ball details in batting team overs for 2nd ball', () => {
    const startingState = {
      battingTeam: {
        overs: [{
          balls: [{ runs: 3, batsmanName: 'Kohli'}],
          bowlerName: 'Sharma'
        }]
      }
    };

    const state = overDetailsReducer(startingState, {
      type: NEXT_BALL_BUTTON_CLICKED,
      payload: {
        currentBall: {
          runs: 1,
          batsmanName: 'Sachin'
        }
      }
    });


    const expectedState = {
      battingTeam: {
        overs: [{
          balls: [
            { runs: 3, batsmanName: 'Kohli'},
            { runs: 1, batsmanName: 'Sachin'}
          ],
          bowlerName: 'Sharma'
        }]
      }
    };
    expect(state.battingTeam).toEqual(expectedState.battingTeam);
  });

  it('should create a new over when player selected action is received', () => {
    const startingState = {
      battingTeam: {
        overs: [{
          balls: Array(6).fill([{ runs: 2, batsmanName: 'Kohli'}]),
          bowlerName: 'Sharma'
        }]
      }
    };

    const state = overDetailsReducer(startingState,
      selectPlayerAction('Srinath', false));


    expect(state.battingTeam.overs[0]).toEqual(startingState.battingTeam.overs[0])
    expect(state.battingTeam.overs[1]).toEqual({
      balls: [],
      bowlerName: 'Srinath',
    });
  });

  it('should not create a new over when player selected action is received but showBattingTeamPlayer is set to true', () => {
    const startingState = {
      battingTeam: {
        overs: [{
          balls: Array(6).fill([{ runs: 2, batsmanName: 'Kohli'}]),
          bowlerName: 'Sharma'
        }]
      }
    };

    const state = overDetailsReducer(startingState,
      selectPlayerAction('Srinath', true));


    expect(state.battingTeam.overs.length).toEqual(1);
  });
});