import { NEXT_BALL_BUTTON_CLICKED, nextInningsAction, nextOverAction } from './currentBallReducer';
import numberOfLegitimateBallsInOver from '../helper/numberOfLegitimateBallsInOver';

const doesOverHave6LegitimateBalls =  (overDetails) => {
  return numberOfLegitimateBallsInOver(overDetails) >= 6;
};

export const checkNextBallMiddleware = store => next => action => {
  const result = next(action);
  const overs = store.getState().overDetails.battingTeam.overs;
  const gameDetails = store.getState().gameDetails;
  if(action.type === NEXT_BALL_BUTTON_CLICKED &&
      doesOverHave6LegitimateBalls(overs[overs.length - 1])) {
    if(overs.length === gameDetails.totalOversInGame) {
      return next(nextInningsAction());
    }
    return next(nextOverAction());
  }
  return result;
};