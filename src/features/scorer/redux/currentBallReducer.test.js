import currentBallReducer, {
  batsmanButtonAction, dispatchNextBallAction,
  initialState,
  nextBallAction, nextOverAction,
  runButtonAction, toggleExtraButtonAction, toggleOutButtonAction
} from './currentBallReducer';
import { BYE, NO_BALL, WIDE } from '../helper/ExtrasConstants';

describe('current ball tests', function () {
  it('should return initial state when state not provided', function () {
    const state = currentBallReducer(undefined, { type: 'REDUX_INIT' });
    expect(state).toEqual(initialState);
  });

  it('should set runs to 5 when runButtonClicked is called with 5', () => {
    const state = currentBallReducer(undefined, runButtonAction(5));
    expect(state.runs).toEqual(5);
  });

  it('should set batsman name to Player 1.1 when batsmanButtonClicked is called with Player 1.1', () => {
    const state = currentBallReducer(undefined, batsmanButtonAction('Player 1.1'));
    expect(state.batsmanName).toEqual('Player 1.1');
  });

  it('should clear current ball when next ball action is fired', () => {
    const state = currentBallReducer({ runs: 4 }, nextBallAction({ runs: 4 }));
    expect(state).toEqual(initialState);
  });

  it('should set WIDE in the array of extras when the extra action is fired', () => {
    const state = currentBallReducer({}, toggleExtraButtonAction(WIDE));
    expect(state).toEqual({
      extras: [WIDE]
    })
  });

  it('should add Byes in the array of extras when the extra action is fired', () => {
    const currentState = { extras: [WIDE] };
    const state = currentBallReducer(currentState, toggleExtraButtonAction(BYE));
    expect(state).toEqual({
      extras: [WIDE, BYE],
    })
  });

  it('should remove WIDE in the array of extras when wide is already set when the extra action is fired', () => {
    const state = currentBallReducer({ extras: [WIDE] }, toggleExtraButtonAction(WIDE));
    expect(state).toEqual({
      extras: []
    })
  });

  it('should set isOut to true when out button action is fired and isOut was not false', () => {
    const state = currentBallReducer({ extras: [] }, toggleOutButtonAction());
    expect(state.isOut).toBeTruthy();
  });

  it('should set isOut to false when out button action is fired and isOut was true', () => {
    const state = currentBallReducer({ extras: [], isOut: true }, toggleOutButtonAction());
    expect(state.isOut).toBeFalsy();
  });
});