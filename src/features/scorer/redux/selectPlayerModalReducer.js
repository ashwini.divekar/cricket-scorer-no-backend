import { NEXT_BALL_BUTTON_CLICKED, NEXT_OVER_ACTION } from './currentBallReducer';

export const SELECT_PLAYER_ACTION = 'Scorer/SELECT_PLAYER_ACTION';

export const selectPlayerAction = (playerName, isBattingTeam) => ({
  type: SELECT_PLAYER_ACTION,
  payload: {
    playerName,
    isBattingTeam,
  }
});

export const initialState = {
  showModal: false,
  showBattingTeamPlayers: false,
};

const selectPlayerModalReducer = (state = initialState, action) => {
  switch(action.type) {
    case NEXT_BALL_BUTTON_CLICKED:
      return action.payload.currentBall.isOut ?
        { showModal: true, showBattingTeamPlayers: true } :
        initialState;
    case NEXT_OVER_ACTION:
      return { showModal: true, showBattingTeamPlayers: false };
    case SELECT_PLAYER_ACTION:
      return initialState;
    default:
      return state;
  }
};

export default selectPlayerModalReducer;