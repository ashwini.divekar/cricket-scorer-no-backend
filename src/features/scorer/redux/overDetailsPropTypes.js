import PropTypes from 'prop-types';
import { CurrentBallPropTypes } from './currentBallPropTypes';

export const OverPropTypes = PropTypes.shape({
  balls: PropTypes.arrayOf(CurrentBallPropTypes),
  bowlerName: PropTypes.string,
});

export const OverDetailsPropTypes = PropTypes.shape({
  battingTeam: PropTypes.shape({
    overs: OverPropTypes.isRequired,
  }),
  bowlingTeam: PropTypes.shape({
    overs: OverPropTypes.isRequired,
  }),
});