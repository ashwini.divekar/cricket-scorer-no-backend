export const WIDE = 'WIDE';
export const NO_BALL = 'NO_BALL';
export const BYE = 'BYE';
export const LEG_BYE = 'LEG_BYE';

export const EXTRAS_THAT_DONT_COUNT_AS_A_BALL = [WIDE, NO_BALL];

export const EXTRA_REPRESENTATION = {
  [WIDE]: 'Wd',
  [NO_BALL]: 'Nb',
  [BYE]: 'By',
  [LEG_BYE]: 'Lb',
};