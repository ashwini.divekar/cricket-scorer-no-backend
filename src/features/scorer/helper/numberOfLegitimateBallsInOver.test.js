import numberOfLegitimateBallsInOver from './numberOfLegitimateBallsInOver';
import { BYE, NO_BALL, WIDE } from './ExtrasConstants';

describe('numberOfLegitimateBallsInOver', () => {
  it('should return the count as 1 if there is 1 legitimate ball', () => {
    expect(numberOfLegitimateBallsInOver({
      balls: [{runs: 0, extras: []}]
    })).toEqual(1);
  });

  it('should return the count as 1 if there is 1 legitimate ball and a wide', () => {
    expect(numberOfLegitimateBallsInOver({
      balls: [
        { runs: 0, extras: [] },
        { runs: 0, extras: [WIDE] },
      ]
    })).toEqual(1);
  });

  it('should return the count as 1 if there is 1 legitimate ball and a wide', () => {
    expect(numberOfLegitimateBallsInOver({
      balls: [
        { runs: 0, extras: [] },
        { runs: 0, extras: [NO_BALL] },
      ]
    })).toEqual(1);
  });

  it('should return the count as 1 if there is 1 legitimate ball and a wide', () => {
    expect(numberOfLegitimateBallsInOver({
      balls: [
        { runs: 1, extras: [BYE] },
      ]
    })).toEqual(1);
  });

});