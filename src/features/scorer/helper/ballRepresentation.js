import { EXTRA_REPRESENTATION } from './ExtrasConstants';

export const WICKET_REPRESENTATION = 'Wkt';

export default (balls) => {
  return balls.map(ball => (
    `${ball.runs === undefined ? '' : ball.runs}`+
    `${(ball.extras || []).map(extra => EXTRA_REPRESENTATION[extra]).join('')}`+
    `${ball.isOut ? WICKET_REPRESENTATION : ''}`
  )).join(' ');
};