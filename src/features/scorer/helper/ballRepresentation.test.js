import ballRepresentation  from './ballRepresentation';
import { BYE, LEG_BYE, NO_BALL, WIDE } from './ExtrasConstants';

describe('ball representation tests', () => {
  it('should return empty string for an empty array', () => {
    expect(ballRepresentation([])).toEqual('');
  });

  it('should return 1 for 1 run', () => {
    expect(ballRepresentation([{
      runs: 1,
      batsman: 'Sachin',
    }])).toEqual('1');
  });

  it('should return Wd for a wide', () => {
    expect(ballRepresentation([{
      runs: 1,
      batsman: 'Sachin',
      extras: [WIDE]
    }])).toEqual('1Wd');
  });

  it('should return Nb for a no ball', () => {
    expect(ballRepresentation([{
      runs: 1,
      batsman: 'Sachin',
      extras: [NO_BALL]
    }])).toEqual('1Nb');
  });

  it('should return By for a bye', () => {
    expect(ballRepresentation([{
      runs: 1,
      batsman: 'Sachin',
      extras: [BYE]
    }])).toEqual('1By');
  });

  it('should return Lb for a leg bye', () => {
    expect(ballRepresentation([{
      runs: 1,
      batsman: 'Sachin',
      extras: [LEG_BYE]
    }])).toEqual('1Lb');
  });

  it('should return 5WdBy for a wide and 4 byes', () => {
    expect(ballRepresentation([{
      runs: 5,
      batsman: 'Sachin',
      extras: [WIDE, BYE]
    }])).toEqual('5WdBy');
  });

  it('should return Wkt is a batsman is out', () => {
    expect(ballRepresentation([{
      runs: 0,
      batsman: 'Sachin',
      isOut: true
    }])).toEqual('0Wkt');
  });

  it('should return 2Wkt is a batsman is run out after scoring 2 runs', () => {
    expect(ballRepresentation([{
      runs: 2,
      batsman: 'Sachin',
      isOut: true
    }])).toEqual('2Wkt');
  });

  it('should return 1 2 1Wd for 3 ball bowled',() => {
    expect(ballRepresentation([{
      runs: 1,
      batsman: 'Sachin',
    }, {
      runs: 2,
      batsman: 'Sachin',
    }, {
      runs: 1,
      batsman: 'Sachin',
      extras: [WIDE]
    }])).toEqual('1 2 1Wd');
  });
});