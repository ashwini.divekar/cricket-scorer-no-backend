import { EXTRAS_THAT_DONT_COUNT_AS_A_BALL } from './ExtrasConstants';

const numberOfLegitimateBallsInOver =  (over) => {
  return over.balls.filter(
    ball => !ball.extras.find(extra => EXTRAS_THAT_DONT_COUNT_AS_A_BALL.indexOf(extra) > -1)
  ).length;
};

export default numberOfLegitimateBallsInOver;
